<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('save-store', 'HomeController@saveStore')->name('save-store');
Route::post('save-product', 'HomeController@saveProduct')->name('save-product');
Route::post('product-autocomplete', 'HomeController@productAutoComplete')->name('product-autocomplete');
Route::post('product-stock-autocomplete', 'HomeController@productStockAutoComplete')->name('product-stock-autocomplete');
Route::get('stock', 'HomeController@stock')->name('stock');
Route::get('sales', 'HomeController@sales')->name('sales');
Route::get('invoice-print/{id}', 'HomeController@invoicePrint')->name('invoice-print');
Route::post('sales-store', 'HomeController@salesStore')->name('sales-store');
