@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <!-- <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }} -->
                <div class="card-header">
                    <h3> Base Product </h3>
                </div>
                <div class="card-body">
                    <form action="{{route('save-product')}}" method="post" id="addProduct">
                        @csrf
                        <input type="hidden" name="id" id="id" value="">
                        <table width="100%">
                            <tr>
                                <td>
                                    <label for="name">Name</label>
                                    <input type="text" name="product_name" id="product_name" class="form-control" value="" required>
                                </td>
                                <td>
                                    <label for="name">SKU</label>
                                    <input type="text" name="sku" id="sku" class="form-control" value="" required>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="name">Product Brand</label>
                                    <select name="product_brand" id="product_brand" class="form-control" required>
                                        <option value="">select</option>
                                        @if(!empty($brand))
                                        @foreach($brand as $value)
                                        <option value="{{$value->id}}">{{$value->brand_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>

                                </td>
                                <td>
                                    <label for="name">Product Category</label>
                                    <select name="product_category" id="product_category" class="form-control" required>
                                        <option value="">select</option>
                                        @if(!empty($category))
                                        @foreach($category as $value)
                                        <option value="{{$value->id}}">{{$value->category_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="name">Description (optional)</label>
                                    <textarea name="description" id="description" class="form-control" cols="3" rows="3"></textarea>
                                </td>
                                <td>
                                    <label for="name">USP (optional)</label>
                                    <textarea name="usp" id="usp" cols="3" rows="3" class="form-control"></textarea>
                                </td>
                            </tr>
                        </table>
                        <div class="text-right mt-2">
                            <button type="button" value="Save" id="data_save" class="btn-success">save</button>

                        </div>
                    </form>
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-header">
                    <h3>Product list</h3>
                </div>
                <div class="card-body">
                    <table id="example" class="table table-striped" width="100%">
                        <thead>
                            <tr>
                                <th>Sl.</th>
                                <th>Name</th>
                                <th>SKU</th>
                                <th>Brand</th>
                                <th>Category</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($product))
                           
                            @foreach($product as $data)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$data->name}}</td>
                                <td>{{$data->sku}}</td>
                                <td>{{$data->brands->brand_name}}</td>
                                <td>{{$data->categorys->category_name}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<!-- Successful Modal -->
<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="successModalLabel">Success</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Your operation was successful!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection
@push('js_right')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script>
    new DataTable('#example');
    $(document).on('click', '#data_save', function() {
        const formData = $('#addProduct').serialize();
        const responseAction = $('#addProduct').attr('action');
        const redir = "{{route('home')}}";
        console.log(redir)
        if (confirm("Are you sure?")) {
            $.ajax({
                async: false,
                url: responseAction,
                data: formData,
                type: "post",
                beforeSend: function() {

                },
                success: function(data) {
                    console.log(data)
                },
                complete: function(data) {
                    $.ajax({
                        async: false,
                        url: redir,
                        type: "get",
                        beforeSend: function() {

                        },
                        success: function(data) {
                            $('input').val('');
                            $('textarea').val('');
                            $('select').val('');
                            $('#successModal').modal('show');
                            $("main").html(data);
                        },
                        complete: function(data) {

                        }
                    });

                }
            });
        }

    })
</script>
@endpush