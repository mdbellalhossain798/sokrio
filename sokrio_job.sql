-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2023 at 07:12 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sokrio_job`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `brand_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `brand_name`, `published_at`, `created_at`, `updated_at`) VALUES
(1, 'Square', '2023-09-16 18:00:00', '2023-09-16 18:00:00', NULL),
(2, 'Acme', '2023-09-16 18:00:00', '2023-09-16 18:00:00', NULL),
(3, 'Pran', '2023-09-16 18:00:00', '2023-09-16 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `published_at`, `created_at`, `updated_at`) VALUES
(1, 'cricket ', '2023-09-16 18:00:00', '2023-09-16 18:00:00', NULL),
(2, 'Vegetable', NULL, NULL, NULL),
(3, 'Kids toy', '2023-09-16 18:00:00', '2023-09-16 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inventory_ind` int(11) NOT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `department_name`, `inventory_ind`, `published_at`, `created_at`, `updated_at`) VALUES
(1, 'Food', 1, '2023-09-16 18:00:00', '2023-09-16 18:00:00', NULL),
(2, 'Sports', 1, '2023-09-16 18:00:00', '2023-09-16 18:00:00', NULL),
(3, 'Dress', 1, '2023-09-16 18:00:00', '2023-09-16 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2023_09_17_140936_create_products_table', 2),
(5, '2023_09_17_141734_create_stock_table', 3),
(6, '2023_09_17_142850_create_sales_table', 4),
(7, '2023_09_17_143528_add_column_products_table', 4),
(8, '2023_09_17_144307_create_brand_table', 4),
(9, '2023_09_17_144512_create_category_table', 4),
(10, '2023_09_17_144745_create_department_table', 4),
(11, '2023_09_17_145221_create_uom_table', 4),
(12, '2023_09_21_140839_sales_mst', 5),
(13, '2023_09_21_141521_add_sales_column', 6),
(14, '2023_09_21_144806_add_sales_column2nd', 7);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usp` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `item_sales_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `sku`, `brand`, `category`, `description`, `usp`, `published_at`, `created_at`, `updated_at`, `item_sales_price`, `item_code`) VALUES
(16, 'Phone', 'p1234', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'dol', '123', 2, 3, 'Description', 'USP', NULL, NULL, NULL, NULL, NULL),
(18, 'Bag', 'B1254', 3, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sales_item_id` bigint(20) UNSIGNED NOT NULL,
  `uom` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `stock_qty` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sales_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `invoice_no` int(11) NOT NULL,
  `item_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `sales_item_id`, `uom`, `department_id`, `stock_qty`, `sales_price`, `published_at`, `created_at`, `updated_at`, `invoice_no`, `item_name`) VALUES
(2, 16, 1, 0, '2', '30', NULL, NULL, NULL, 7, 'Phone'),
(3, 16, 1, 0, '2', '30', NULL, NULL, NULL, 8, 'Phone'),
(4, 16, 1, 0, '2', '30', NULL, NULL, NULL, 9, 'Phone'),
(5, 16, 1, 0, '2', '30', NULL, NULL, NULL, 10, 'Phone'),
(6, 16, 1, 0, '2', '30', NULL, NULL, NULL, 11, 'Phone'),
(7, 16, 1, 0, '2', '40', NULL, NULL, NULL, 12, 'Phone'),
(8, 16, 1, 0, '2', '30', NULL, NULL, NULL, 13, 'Phone'),
(9, 16, 1, 2, '2', '2', NULL, NULL, NULL, 14, 'Phone'),
(10, 17, 1, 2, '2', '20', NULL, NULL, NULL, 16, 'dol'),
(11, 17, 1, 2, '3', '30', NULL, NULL, NULL, 17, 'dol'),
(12, 16, 2, 2, '2', '10', NULL, NULL, NULL, 17, 'Phone');

-- --------------------------------------------------------

--
-- Table structure for table `salesmst`
--

CREATE TABLE `salesmst` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_no` int(11) NOT NULL,
  `grand_total` bigint(20) NOT NULL,
  `payment` bigint(20) NOT NULL,
  `due` bigint(20) NOT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salesmst`
--

INSERT INTO `salesmst` (`id`, `invoice_no`, `grand_total`, `payment`, `due`, `published_at`, `created_at`, `updated_at`) VALUES
(7, 0, 30, 30, 0, NULL, NULL, NULL),
(8, 0, 30, 20, 10, NULL, NULL, NULL),
(9, 0, 30, 20, 10, NULL, NULL, NULL),
(10, 0, 30, 30, 0, NULL, NULL, NULL),
(11, 0, 30, 3, 27, NULL, NULL, NULL),
(12, 0, 40, 30, 10, NULL, NULL, NULL),
(13, 0, 30, 30, 0, NULL, NULL, NULL),
(14, 0, 20, 20, 0, NULL, NULL, NULL),
(15, 0, 20, 20, 0, NULL, NULL, NULL),
(16, 0, 20, 15, 5, NULL, NULL, NULL),
(17, 0, 40, 30, 10, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` int(11) NOT NULL,
  `challan_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock_item_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock_sku` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock_qty` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `item_id`, `department_id`, `challan_no`, `stock_item_name`, `stock_sku`, `stock_qty`, `published_at`, `created_at`, `updated_at`) VALUES
(1, 17, 2, 's1254', 'dol', '123', '2', NULL, NULL, NULL),
(2, 16, 2, 's2546', 'Phone', 'p1234', '3', NULL, NULL, NULL),
(5, 16, 1, 'f123', 'Phone', 'p1234', '3', NULL, NULL, NULL),
(6, 17, 1, 'f123', 'dol', '123', '6', NULL, NULL, NULL),
(7, 16, 3, 'd123', 'Phone', 'p1234', '9', NULL, NULL, NULL),
(8, 17, 3, 'd123', 'dol', '123', '1', NULL, NULL, NULL),
(9, 16, 1, 'f123', 'Phone', 'p1234', '3', NULL, NULL, NULL),
(10, 17, 1, 'f123', 'dol', '123', '6', NULL, NULL, NULL),
(11, 16, 2, NULL, 'Phone', NULL, '-2', NULL, NULL, NULL),
(12, 16, 2, NULL, 'Phone', NULL, '-2', NULL, NULL, NULL),
(13, 17, 2, NULL, 'dol', NULL, '-2', NULL, NULL, NULL),
(14, 17, 2, NULL, 'dol', NULL, '-3', NULL, NULL, NULL),
(15, 16, 2, NULL, 'Phone', NULL, '-2', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `uom`
--

CREATE TABLE `uom` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uom_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uom_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `uom`
--

INSERT INTO `uom` (`id`, `uom_name`, `uom_code`, `published_at`, `created_at`, `updated_at`) VALUES
(1, 'piece', 'piece', '2023-09-16 18:00:00', '2023-09-16 18:00:00', NULL),
(2, 'Box', 'Box', '2023-09-16 18:00:00', '2023-09-16 18:00:00', NULL),
(3, 'Kg', 'Kg', '2023-09-16 18:00:00', '2023-09-16 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'bellal', 'admin@app.com', NULL, '$2y$10$w9q6vue9QolQpuvVMTLqRu.mFuv2zDiRpkEWmXOWrwJ6XWvpwe/jC', NULL, '2023-09-17 07:50:40', '2023-09-17 07:50:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salesmst`
--
ALTER TABLE `salesmst`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stock_item_id_foreign` (`item_id`);

--
-- Indexes for table `uom`
--
ALTER TABLE `uom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `salesmst`
--
ALTER TABLE `salesmst`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `uom`
--
ALTER TABLE `uom`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `stock_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `products` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
