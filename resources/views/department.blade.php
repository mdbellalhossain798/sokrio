@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3> Stock </h3>
                </div>
                <div class="card-body">
                    <form action="{{route('save-store')}}" method="post" id="stock_add">
                        @csrf
                        <input type="hidden" name="id" id="id" value="">
                        <table width="100%">
                            <tr>
                                <td>
                                    <label for="department">Department</label>
                                    <select name="department" id="department" class="form-control" required>
                                        <option value="">select</option>
                                        @if(!empty($department))
                                        @foreach($department as $value)
                                        <option value="{{$value->id}}">{{$value->department_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </td>
                                <td>
                                    <label for="challan_no">Challan No</label>
                                    <input type="text" name="challan_no" id="challan_no" class="form-control" value="" required>
                                </td>
                            </tr>

                        </table>
                        <table width="100%">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>SKU</th>
                                    <th>Add Qty.</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th>
                                        <input type="text" name="" id="product_name" class="form-control" autocomplete="off">
                                        <ul id="product-list"></ul>
                                        <input type="hidden" name="" id="product_id">
                                    </th>
                                    <th><input type="text" name="" id="sku" class="form-control" readonly></th>
                                    <th><input type="number" name="" id="qty" class="form-control" min="1" autocomplete="off"></th>
                                    <th><button class="form-control btn btn-info" type="button" id="add_product" onclick="addProduct(this)">Add</button></th>
                                </tr>
                            </thead>
                            <tbody id="table_body">

                            </tbody>


                        </table>
                        <div class="text-right mt-2">
                            <button type="submit" value="Save" id="data_save" class="btn-success">save</button>

                        </div>
                    </form>
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-header">
                    <h3>Product Stock list</h3>
                </div>
                <div class="card-body">
                    <table id="example" class="table table-striped" width="100%">
                        <thead>
                            <tr>
                                <th>Sl.</th>
                                <th>Name</th>
                                <th>SKU</th>
                                <th>Stock Qty</th>
                                <th>Department</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($stock))
                           
                            @foreach($stock as $data)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$data->stock_item_name}}</td>
                                <td>{{$data->stock_sku}}</td>
                                <td>{{$data->stock_qty}}</td>
                                <td>{{$data->departments->department_name}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<!-- Successful Modal -->
<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="successModalLabel">Success</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Your operation was successful!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection
@push('js_right')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script>
    function addProduct(element) {
        var product_name = $(element).parents('tr').find('#product_name').val();
        var product_id = $(element).parents('tr').find('#product_id').val();
        var sku = $(element).parents('tr').find('#sku').val();
        var qty = $(element).parents('tr').find('#qty').val();

        if (qty < 1 || product_name == "") {
            alert("Enter Data");
            return false;
        }
        var row = ` <tr><th><input type="text" name="product_name[]" id="product_name" class="form-control" value="${product_name}"><input type="hidden" name="product_id[]" id="product_id" value="${product_id}"></th><th><input type="text" name="sku[]" id="sku" class="form-control" value="${sku}" readonly></th><th><input type="number" name="qty[]" id="qty" class="form-control" min="1" value="${qty}"></th><th><button class="form-control btn btn-danger" type="button" id="add_product" onclick="removeProduct(this)">Remove</button></th></tr>`;

        $('#table_body').append(row);
        $(element).parents('tr').find('#product_name').val('');
        $(element).parents('tr').find('#product_id').val('');
        $(element).parents('tr').find('#sku').val('');
        $(element).parents('tr').find('#qty').val('');

    }

    function removeProduct(element) {
        $(element).parents('tr').remove();
    }

    $(document).ready(function() {
        $("#product_name").on("input", function() {
            var query = $(this).val();
            console.log(query);
            if (query !== "") {
                $.ajax({
                    url: "{{ route('product-autocomplete') }}", // Use the route helper to get the correct URL
                    type: "POST",
                    data: {
                        query: query,
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(response) {
                        var productList = $("#product-list");
                        productList.empty();

                        var products = JSON.parse(response);

                        if (products.length > 0) {
                            products.forEach(function(product) {
                                var listItem = $("<li>" + product.name + "</li>");
                                listItem.click(function() {
                                    // Set the selected product name in the input field
                                    $("#product_name").val(product.name);
                                    $("#product_id").val(product.id);
                                    $("#sku").val(product.sku);
                                    productList.empty(); // Clear the suggestion list
                                });
                                productList.append(listItem);
                            });
                        } else {
                            productList.append("<li>No products found.</li>");
                        }
                    }
                });
            } else {
                $("#product-list").empty();
            }
        });
    });


    new DataTable('#example');
    $(document).on('click', '#data_save', function() {
        const formData = $('#stock_add').serialize();
        const responseAction = $('#stock_add').attr('action');
        const redir = "{{route('stock')}}";
       
        if ($('#department').val() =="" || $('#challan_no').val() == "") {
            alert("select department or challan ");
            return false;
        }
        console.log(redir, $('#table_body').find('tr').length )
        if ($('#table_body').find('tr').length < 1 ) {
            alert("select item");
            return false;
        }
        
        if (confirm("Are you sure?")) {
            $.ajax({
                async: false,
                url: responseAction,
                data: formData,
                type: "post",
                beforeSend: function() {

                },
                success: function(data) {
                    console.log(data)
                },
                complete: function(data) {
                    $.ajax({
                        async: false,
                        url: redir,
                        type: "get",
                        beforeSend: function() {

                        },
                        success: function(data) {
                            $('input').val('');
                            $('textarea').val('');
                            $('select').val('');
                            $('#successModal').modal('show');
                            $("main").html(data);
                        },
                        complete: function(data) {
                            $('#table_body').html('');
                        }
                    });

                }
            });
        }

    })
</script>
@endpush