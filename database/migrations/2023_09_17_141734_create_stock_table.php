<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock', function (Blueprint $table) {
            $table->id(); // Primary Key
            $table->unsignedBigInteger('item_id'); 
            $table->integer('department_id');
            $table->string('challan_no');
            $table->string('stock_item_name');
            $table->string('stock_sku');
            $table->string('stock_qty');
            $table->timestamp('published_at')->nullable();
            $table->timestamps();
            $table->foreign('item_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock');
    }
}
