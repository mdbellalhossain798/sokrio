<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('item_sales_price')->nullable(); 
            $table->string('item_code')->nullable(); 
        });
       
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('item_sales_price'); // Remove the "email" column if you ever need to rollback
            $table->dropColumn('item_code'); // Remove the "email" column if you ever need to rollback
        });
    }
}
