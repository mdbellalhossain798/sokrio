<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id(); // Primary Key
            $table->unsignedBigInteger('sales_item_id'); 
            $table->integer('uom');
            $table->integer('department_id');
            $table->string('stock_qty');
            $table->string('sales_price');
            $table->timestamp('published_at')->nullable();
            $table->timestamps();
            // $table->foreign('sales_item_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
