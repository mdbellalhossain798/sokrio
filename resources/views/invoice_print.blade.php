
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="" />
    <!-- custom style -->


    <style>
        @page {
            padding-bottom: 100px;
        }

        @font-face {
            font-family: 'IBM Plex Sans,Helvetica,Arial,sans-serif';
            font-weight: normal;
            font-size: 10px;
            font-style: normal;
            font-variant: normal;
            src: url("font url");
        }

        body {
            font-family: 'IBM Plex Sans,Helvetica,Arial,sans-serif';
            font-size: 12px;
        }

        footer {
            bottom: 0;
            left: 0cm;
            right: 0cm;
        }

        .signature_ {
            position: fixed;
            bottom: 1.6cm;
            left: .8cm;
            right: .8cm;
            width: 100%;
        }

        .tr_border td,
        .tr_border th {
            border: 1px solid #222;
        }
    </style>

</head>

<body>

    <table style="width: 100%;border-collapse:collapse;" cellpadding="1" cellspacing="0">
        <thead>
            <tr>
                <td colspan="12" style="">
                    <table style="width: 100%; border:none;" class="signature">
                        <tr>
                           
                            <td style="text-align:center; border:none;">
                                <span style="font-size: 15px;"> <strong>
                                        Sokrio
                                    </strong></span>
                                <br>
                                Address:
                                <span>
                                  </span>Dhaka<br>
                                Phone No:
                                <span>
                                    01996548</span>
                            </td>
                           
                        </tr>
                    </table>
                    <div style=""> 
                    <table>
                        <tr>
                            <td>
                                <strong>Invoice No : {{$salesMst->id}}</strong> 
                            </td>
                           
                        </tr>
                    </table>
                </div>
                </td>
            </tr>
            <tr class="tr_border">
                <th class="bg-soft-green text-center">SL#</th>
                <th class="bg-soft-green text-left" scope="col">itam Name</th>
                <th class="bg-soft-green text-center" scope="col">Qty</th>
                <th class="bg-soft-green text-center" scope="col">Unit</th>
                <th class="bg-soft-green text-right" scope="col">Amount <small>(BDT)</small></th>
                
            </tr>
        </thead>
        <tbody>

            @if(!empty($sales))

            @foreach($sales as $key=>$row)
            <tr class="tr_border">
                <td class="border-1 bg-light text-center  " style="text-align: center;">{{ $loop->iteration }}</td>
             
                <td class="border-1 bg-light text-left "><strong>{{ $row->item_name }}</strong></td>
                <td class="border-1 bg-light text-center " style="text-align: center;">{{ $row->stock_qty }}</td>
                <td class="border-1 bg-light text-center "style="text-align: center;">{{ $row->uoms->uom_name }}</td>
                <td class="border-1 bg-light text-right " style="text-align: right;">{{ $row->sales_price }}</td>

            </tr>
            @endforeach
           
            <tr>
                <td colspan="4" class="text-right" style="text-align: right;">Grand Total : </td>
                <td class="text-right" style="text-align: right;">{{$salesMst->grand_total}}</td>
            </tr>
            <tr>
                <td colspan="4" class="text-right" style="text-align: right;">Pay : </td>
                <td class="text-right" style="text-align: right;">{{$salesMst->payment}}</td>
            </tr>
            <tr>
                <td colspan="4" class="text-right" style="text-align: right;">Due : </td>
                <td class="text-right" style="text-align: right;">{{$salesMst->due}}</td>
            </tr>
            @endif
        </tbody>

        <tfoot>
            <tr>
                <td style="width: 100%; " colspan="12">
                    <table style="width: 100%; ">
                        <tr>
                            <td style="font-size: 9px; text-align:left;border:none;">Print Date & Time: <strong>
                                    <?php
                                    $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                                    $date = $dt->format('g:i A');
                                    ?>
                                    {{ date('d/m/Y') }} {{ $date }}
                                </td>
                            <td style="font-size: 9px; text-align: right; border:none;"><strong>Powered By - Sokrio
                                </strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tfoot>
    </table>
    <script>
        window.onload = function() {
            window.print();
        }
    </script>
</body>

</html>