<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SalesMst extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salesMst', function (Blueprint $table) {
            $table->id(); // Primary Key          
            $table->integer('invoice_no');
            $table->bigInteger('grand_total');
            $table->bigInteger('payment');
            $table->bigInteger('due');
            $table->timestamp('published_at')->nullable();
            $table->timestamps();

            // Define the foreign key constraint
            // $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
