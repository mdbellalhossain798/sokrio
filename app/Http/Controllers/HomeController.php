<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Department;
use App\Product;
use App\sale;
use App\Stock;
use App\Uom;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\TryCatch;
use App\SalesMst;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $brand=Brand::get();
        $category=Category::get();
        $product=Product::with(['brands', 'categorys'])->get();
        // dd( $product);
        return view('home',['brand'=>$brand,'category'=>$category,'product'=>$product]);
    }
    public function saveProduct( Request $request){
      
        try {
            $data=[
                'name'=>$request->product_name,
                'sku'=>$request->sku,
                'brand'=>$request->product_brand,
                'category'=>$request->product_category,
                'description'=>$request->description,
                'usp'=>$request->usp
            ];
            if ($request->id =="") {
                // dd($data);
                Product::insert($data);
            }
          
            DB::commit();
            $message= ['msg' => 'Success.', 'title' => 'Success'];
        } catch (\Throwable $th) {
            DB::rollback();
            $message = ['msg' => $th , 'title' => 'Error'];
        }
        return response()->json($message);
        // return redirect()->route('home')
        // ->with('success', 'Your operation was successful!');
    }

    public function stock()
    {
       
        $department=Department::get();
        $stock=Stock::with('departments')->select(DB::raw('sum(stock_qty) as stock_qty'),'stock_item_name','department_id')->groupBy('stock_item_name','department_id')->orderBy('stock_item_name','asc')->get();
        // dd( $stock);
        return view('department',['department'=>$department,'stock'=>$stock]);
    }

    public function productAutoComplete(Request $request){

        $key_word=$_POST["query"];
        $products = Product::where('name', 'like', '%' . $key_word . '%')->get();
     
        
        // dd(  $products);
        $filteredProducts = [];
        
        foreach ($products as $product) {
            if (stripos($product["name"], $key_word) !== false) {
                $filteredProducts[] = $product;
            }
        }
        echo json_encode($filteredProducts);
     
    }
    public function productStockAutoComplete(Request $request){

        $key_word=$_POST["query"];
        $products = Stock::select(DB::raw('sum(stock_qty) as stock_qty'),'stock_item_name','department_id','item_id')->where('stock_item_name', 'like', '%' . $key_word . '%')->groupBy('stock_item_name','department_id','item_id')->get();
     
        
        // dd(  $products);
        $filteredProducts = [];
        
        foreach ($products as $product) {
            if (stripos($product["stock_item_name"], $key_word) !== false) {
                $filteredProducts[] = $product;
            }
        }
        // dd($filteredProducts);
        echo json_encode($filteredProducts);
     
    }
    public function saveStore( Request $request){   
        try {
            if (count($request->product_id)>0) {
                for ($i=0; $i < count($request->product_id); $i++) { 
                    $data=[
                        'item_id'=>$request->product_id[$i],
                        'department_id'=>$request->department,
                        'challan_no'=>$request->challan_no,
                        'stock_item_name'=>$request->product_name[$i],
                        'stock_sku'=>$request->sku[$i],
                        'stock_qty'=>$request->qty[$i]
                    ];                   
                        // dd($data);
                        Stock::insert($data);
                    }
              
                DB::commit();
            $message= ['msg' => 'Success.', 'title' => 'Success'];
            }
            
          
            
        } catch (\Throwable $th) {
            DB::rollback();
            $message = ['msg' => $th , 'title' => 'Error'];
        }
        return response()->json($message);
        // return redirect()->route('home')
        // ->with('success', 'Your operation was successful!');
    }
    public function sales()
    {
       
        $uom=Uom::get();
        $stock=Stock::with('departments')->select(DB::raw('sum(stock_qty) as stock_qty'),'stock_item_name','stock_sku','department_id')->groupBy('stock_item_name','stock_sku','department_id')->orderBy('stock_item_name','asc')->get();
        // dd( $stock);
        return view('sales',['uom'=>$uom]);
    }
    public function salesStore( Request $request){  
       
       
        try {
            $last_invoice=SalesMst::orderBy('id','desc')->first();
            $mstData=[
                'invoice_no'=>0  ,
                'grand_total'=>$request->grand_total ? $request->grand_total:0  ,
                'payment'=>$request->pay ?$request->pay:0 ,
                'due'=>$request->due ?$request->due:0 ,
            ];
            $mstSave=SalesMst::insertGetId($mstData);
            
            
            if (count($request->product_id)>0) {
                for ($i=0; $i < count($request->product_id); $i++) { 
                    $data=[
                        'invoice_no'=> $mstSave,
                        'sales_item_id'=>$request->product_id[$i],
                        'uom'=>$request->uom[$i],
                        'sales_price'=>$request->total_amt[$i],
                        'item_name'=>$request->product_name[$i],
                        'stock_qty'=>$request->qty[$i],
                        'department_id'=>$request->department_id,
                    ];                   
                        // dd($data);
                        sale::insert($data);
                    }
              
              
            }

            if (count($request->product_id)>0) {
                for ($i=0; $i < count($request->product_id); $i++) { 
                    $stock=[
                        'item_id'=>$request->product_id[$i],
                        'department_id'=>$request->department_id,                      
                        'stock_item_name'=>$request->product_name[$i],
                        'stock_qty'=>(0-$request->qty[$i])
                    ];                   
                        // dd($data);
                        Stock::insert($stock);
                    }            
              
            }
            DB::commit();
            $message= ['msg' => 'Success.', 'id'=>$mstSave,'title' => 'Success'];
          
            
        } catch (\Throwable $th) {
            DB::rollback();
            $message = ['msg' => $th , 'title' => 'Error'];
        }
        return response()->json($message);
        // return redirect()->route('home')
        // ->with('success', 'Your operation was successful!');
    }

    public function invoicePrint($id){
        $salesMst=SalesMst::where('id',$id)->first();
        $sales=sale::with('uoms')->where('invoice_no',$id)->get();
// dd( $sales);
        return view('invoice_print',['salesMst'=>$salesMst,'sales'=>$sales]);
    }
}
