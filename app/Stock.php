<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    
    protected $table = 'stock';

    public function departments()
    {
        return $this->belongsTo(Department::class, 'department_id');
    }
}
