@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3> Sales </h3>
                </div>
                <div class="card-body">
                    <form action="{{route('sales-store')}}" method="post" id="sales_genarate">
                        @csrf
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="department_id" id="department_id" value="">
                       
                        <table width="100%">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Qty</th>
                                    <th>Unit</th>
                                    <th>Sales Price</th>
                                    <th>Total Amt.</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th>
                                        <input type="text" name="" id="product_name" class="form-control" autocomplete="off">
                                        <ul id="product-list"></ul>
                                        <input type="hidden" name="" id="product_id">
                                    </th>
                                    <th><input type="number" name="" id="qty" class="form-control" min="1" autocomplete="off"></th>
                                    <th>
                                    <select name="" id="unit" class="form-control" required>
                                        <option value="">select</option>
                                        @if(!empty($uom))
                                        @foreach($uom as $value)
                                        <option value="{{$value->id}}">{{$value->uom_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    </th>
                                    <th><input type="number" name="" id="price" class="form-control" min="1" autocomplete="off" onkeyup="setPrice(this)"></th>
                                    <th><input type="number" name="" id="total_amt" class="form-control" min="1" autocomplete="off" readonly></th>
                                    <th><button class="form-control btn btn-info" type="button" id="add_product" onclick="addProduct(this)">Add</button></th>
                                </tr>
                            </thead>
                            <tbody id="table_body">

                            </tbody>
                            <tfoot id="">
                                <tr>                                    
                                    <td colspan="4" class="text-right " style="font-style:bold">Grand Total : </td>
                                    <td id="" class="text-right " style="font-style:bold">
                                    <input type="number" name="grand_total" id="grand_total" class="form-control text-right" value="0" readonly>
                                </td>
                                    <td></td>
                                </tr>
                                <tr>                                    
                                    <td colspan="4" class="text-right " style="font-style:bold">Pay : </td>
                                    <td id="" class="text-right " style="font-style:bold"><input type="number" name="pay" value="0" id="pay" class="form-control text-right" onkeyup="payment(this)"></td>
                                    <td></td>
                                </tr>
                                <tr>                                    
                                    <td colspan="4" class="text-right " style="font-style:bold">Due : </td>
                                    <td id="" class="text-right " style="font-style:bold">
                                    <input type="number" name="due" id="due" class="form-control text-right" value="0" readonly>
                                </td>
                                    <td></td>
                                </tr>
                            </tfoot>


                        </table>
                        <div class="text-right mt-2">
                            <button type="submit" value="Save" id="data_save" class="btn-success">save</button>

                        </div>
                    </form>
                </div>
            </div>
            <!-- <div class="card mt-2">
                <div class="card-header">
                    <h3>Product Stock list</h3>
                </div>
                <div class="card-body">
                    <table id="example" class="table table-striped" width="100%">
                        <thead>
                            <tr>
                                <th>Sl.</th>
                                <th>Name</th>
                                <th>SKU</th>
                                <th>Stock Qty</th>
                                <th>Department</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($stock))
                           
                            @foreach($stock as $data)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$data->stock_item_name}}</td>
                                <td>{{$data->stock_sku}}</td>
                                <td>{{$data->stock_qty}}</td>
                                <td>{{$data->departments->department_name}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>

                    </table>
                </div>

            </div> -->
        </div>
    </div>
</div>
</div>
<!-- Successful Modal -->
<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="successModalLabel">Success</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="message_body"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection
@push('js_right')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script>
    function addProduct(element) {
        var product_name = $(element).parents('tr').find('#product_name').val();
        var product_id = $(element).parents('tr').find('#product_id').val();
        var price = $(element).parents('tr').find('#price').val();
        var qty = $(element).parents('tr').find('#qty').val();
        var unit = $(element).parents('tr').find('#unit').val();
        var total_amt = $(element).parents('tr').find('#total_amt').val();

        if (qty < 1 || product_name == ""|| price =="" || unit=="") {
            alert("Enter All Data");
            return false;
        }
        var row = ` <tr><th><input type="text" name="product_name[]" id="product_name" class="form-control" value="${product_name}"><input type="hidden" name="product_id[]" id="product_id" value="${product_id}"></th><th><input type="number" name="qty[]" id="qty" class="form-control" min="1" value="${qty}"></th><th><input type="text" name="uom[]" id="uom" class="form-control" value="${unit}" ></th><th><input type="number" name="price[]" id="price" class="form-control text-right" value="${price}" min='1' ></th><th><input type="number" name="total_amt[]" id="total_amt" class="form-control text-right price" min="1" readonly value="${total_amt}" autocomplete="off"></th><th><button class="form-control btn btn-danger" type="button" id="add_product" onclick="removeProduct(this)">Remove</button></th></tr>`;

        $('#table_body').append(row);
        grand_total();
        $(element).parents('tr').find('#product_name').val('');
        $(element).parents('tr').find('#product_id').val('');
        $(element).parents('tr').find('#price').val('');
        $(element).parents('tr').find('#qty').val('');
        $(element).parents('tr').find('#unit').val('');
        $(element).parents('tr').find('#total_amt').val('');

    }

    function setPrice(element){
        var qty = $(element).parents('tr').find('#qty').val();
        var price = $(element).parents('tr').find('#price').val();
        $('#total_amt').val(qty*price);
    }

    function payment(element){
        var pay_amt=parseFloat($(element).val());
        var total_price=0;
        jQuery('.price').each(function(index, currentElement) {
            total_price +=parseFloat($(currentElement).val());
        });
        if (total_price < pay_amt) {
            alert("Incorrect payment");
            $(element).val('')
            $('#due').val('');
            return false;
        }
        $('#due').val( total_price - pay_amt)

    }
    function grand_total(){
        var total_price=0;
        jQuery('.price').each(function(index, currentElement) {
            total_price +=parseFloat($(currentElement).val());
        });
        $('#grand_total').val(total_price);
    }
    function removeProduct(element) {
        $(element).parents('tr').remove();
        grand_total();
    }

    $(document).ready(function() {
        $("#product_name").on("input", function() {
            var query = $(this).val();
            console.log(query);
            if (query !== "") {
                $.ajax({
                    url: "{{ route('product-stock-autocomplete') }}", // Use the route helper to get the correct URL
                    type: "POST",
                    data: {
                        query: query,
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(response) {
                        var productList = $("#product-list");
                        productList.empty();

                        var products = JSON.parse(response);
                        console.log(response,products,products.length +'_____bellal');
                        if (products.length > 0) {
                            products.forEach(function(product) {
                                var listItem = $("<li>" + product.stock_item_name + "  ( Department id : "+ product.department_id+" ) </li>");
                                listItem.click(function() {
                                    // Set the selected product name in the input field
                                    $("#product_name").val(product.stock_item_name);
                                    $("#product_id").val(product.item_id );
                                    $("#department_id").val(product.department_id);
                                    productList.empty(); // Clear the suggestion list
                                });
                                productList.append(listItem);
                            });
                        } else {
                            productList.append("<li>No products found.</li>");
                        }
                    }
                });
            } else {
                $("#product-list").empty();
            }
        });
    });


    new DataTable('#example');
    $(document).on('click', '#data_save', function() {
        const formData = $('#sales_genarate').serialize();
        const responseAction = $('#sales_genarate').attr('action');
        const redir = "{{route('sales')}}";
       
        if ($('#department').val() =="" || $('#challan_no').val() == "") {
            alert("select department or challan ");
            return false;
        }
        console.log(redir, $('#table_body').find('tr').length )
        if ($('#table_body').find('tr').length < 1 ) {
            alert("select item");
            return false;
        }
        
        if (confirm("Are you sure?")) {
            $.ajax({
                async: false,
                url: responseAction,
                data: formData,
                type: "post",
                beforeSend: function() {

                },
                success: function(data) {
                   var id=data.id;
                  
                   var print_url= `<br><a class="" href="{{ route('invoice-print', ['id' => ':id']) }}" target="_blank" rel="noopener noreferrer">Prind Invoice</a>`;
                   print_url = print_url.replace(':id', id);
                   $('#message_body').html(`${data.msg} <strong>Invoce id : ${data.id}</strong>${print_url} `);
                    console.log(data)
                },
                complete: function(data) {
                    $.ajax({
                        async: false,
                        url: redir,
                        type: "get",
                        beforeSend: function() {

                        },
                        success: function(data) {
                           
                            $('input').val('');
                            $('textarea').val('');
                            $('select').val('');
                          
                            $('#successModal').modal('show');
                            $("main").html(data);
                        },
                        complete: function(data) {
                            $('#table_body').html('');
                        }
                    });

                }
            });
        }

    })
</script>
@endpush