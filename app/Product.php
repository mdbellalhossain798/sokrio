<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    
    public function stocks()
    {
        return $this->hasMany(Stock::class, 'item_id');
    }
    public function sales()
    {
        return $this->hasMany(sale::class, 'sales_item_id');
    }

    public function brands()
    {
        return $this->belongsTo(Brand::class, 'brand'); // 'brand_id' should be the foreign key in the products table
    }

    // Define the relationship with Category (assuming one-to-many)
    public function categorys()
    {
        return $this->belongsTo(Category::class, 'category'); // 'category_id' should be the foreign key in the products table
    }


   
    const CREATED_AT = 'create_at';
}
