<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sale extends Model
{
    protected $table='sales';

    public function uoms()
    {
        return $this->belongsTo(Uom::class, 'uom');
        
    }

   
}
